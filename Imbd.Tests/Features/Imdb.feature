﻿Feature: Movie
    Simple imdb like project for adding and displaying movies



@addmovies
Scenario: Add Movies
    Given the name of movie is "f2"
    And the year of release is 2000
    And the plot of movie is "sample plot"
    And the actors are '1,2'
    And producer is 1
    When the user added movie
    Then the result will be like
    | Name | Year | Plot        | Producer |
    | f2   | 2000 | sample plot | Raju     |
    And the actor list would be like
    | MovieName | Actors   |
    | f2   | dfs,rdfs |
@listmovies
 Scenario: List movie
    Given the list of movies
    When the user fetches movies
    Then the result should show like this
    | Name | Year | Plot        | Producer |
    | kgf  | 1998 | sample plot | Raju     |
    
    And the actors list is like
    | MovieName | Actor    |
    | kgf  | dfs,rdfs |
    

    
   
   
