﻿using System;
using System.Collections.Generic;
using System.Linq;
using Imdb.Domain;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests.Features
{
    [Binding]
    public class ImdbSteps
    {
        private MovieService _moviesservice;
        private ActorService _actorservice;
        private ProducerService _producerservice;
        private string _name,_plot,_producer;
        private int _year;

        private List<string> _actors;
        private List<Movie> _movies;
        public ImdbSteps()
        {
            _moviesservice = new MovieService();
            _actorservice = new ActorService();
            _producerservice = new ProducerService();
            _actors = new List<string>();
            

        }
        [Given(@"the name of movie is ""(.*)""")]
        public void GivenTheNameOfMovieIs(string name)
        {
            _name = name;
        }

        [Given(@"the year of release is (.*)")]
        public void GivenTheYearOfReleaseIs(int year)
        {
            _year = year;
        }

        [Given(@"the plot of movie is ""(.*)""")]
        public void GivenThePlotOfMovieIs(string plot)
        {
            _plot = plot;
        }
        


        [Given(@"the actors are ""(.*)""")]
        public void GivenTheActorsAre(Decimal p0)
        {
            string str = p0.ToString();
           
            List<char> ch = str.ToCharArray().ToList();
            int[] actorIndexes = ch.Select(a => a - '0').ToArray();
            var actorList = _actorservice.GetActors();
            //var givenActorsList = new List<Actor>();
            foreach (var actorIndex in actorIndexes)
            {
                _actors.Add(actorList[actorIndex - 1].Name);
            }
        }
           

        [Given(@"producer is (.*)")]
        public void GivenProducerIs(int p0)
        {
            _producer = _producerservice.GetProducers()[p0 - 1].Name;
        }

        [When(@"the user added movie")]
        public void WhenTheUserAddedMovie()
        {
            _moviesservice.AddMovie(_name, _year, _plot, _actors, _producer);
            _movies = _moviesservice.GetMovies();
        }

        [Then(@"the result will be like")]
        public void ThenTheResultWillBeLike(Table table)
        {
            var account = table.CreateSet<Movie>();

            foreach (var i in account)
            {
                foreach (var movie in _movies)
                {
                    Assert.AreEqual(i.Name, movie.Name);


                }
            }

        }

        [Then(@"the actor list would be like")]
        public void ThenTheActorListWouldBeLike(Table table)
        {
            var account = table.CreateSet<(string Name, List<string> Actors)>();
            
            foreach (var i in account)
            {
                foreach (var movie in _movies)
                {
                    Assert.AreEqual(i.Name,movie.Name);
                    Assert.AreEqual(i.Actors, movie.Actors);
                }
            }

        }


        [Given(@"the list of movies")]
        public void GivenTheListOfMovies()
        {

        }

        [When(@"the user fetches movies")]
        public void WhenTheUserFetchesMovies()
        {
            _movies = _moviesservice.GetMovies();

        }

        [Then(@"the result should show like this")]
        public void ThenTheResultShouldShowLikeThis(Table table)
        {
            var account = table.CreateSet<Movie>();

            foreach (var i in account)
            {
                foreach (var movie in _movies)
                {
                    Assert.AreEqual(i.Name,movie.Name);
                }
            }
        }
        [Then(@"the actors list is like")]
        public void ThenTheActorsListIsLike(Table table)
        {
            var account = table.CreateSet<(string Name, List<string> Actors)>();
            int index = 0;
            foreach (var i in account)
            {
                foreach (var movie in _movies)
                {
                    Assert.AreEqual(i.Name, movie.Name);
                    Assert.AreEqual(i.Actors, movie.Actors);
                }
            }

        }

        [BeforeScenario("addmovies")]
        public void AddSampleActorForAdd()
        {
            _actorservice.AddActor("Actor1", new DateTime(2000, 03, 15));
            _actorservice.AddActor("Actor2", new DateTime(2000, 03, 15));
            _producerservice.AddProducer("Producer1", new DateTime(2000, 03, 15));
            


        }

        [BeforeScenario("listmovies")]
        public void AddSampleBookForAdd()
        {
            _moviesservice.AddMovie("Movie1", 1998, "sample plot", new List<string>(){
                "Actor1",
                "Actor2"
            }, "Producer1");
            _moviesservice.AddMovie("Movie2", 1998, "sample plot", new List<string>(){
                "Actor1",
                "Actor2"
            }, "Producer1");


        }
    }
}
