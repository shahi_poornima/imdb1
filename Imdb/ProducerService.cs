﻿using System;
using System.Collections.Generic;
using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    public class ProducerService
    {
        private readonly ProducerRepository _producerRepository;

        public ProducerService()
        {
            _producerRepository = new ProducerRepository();
        }

        public void AddProducer(string name, DateTime dob)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            var producer = new Producer()
            {
                Name = name,
                Dob = dob
            };

            _producerRepository.Add(producer);
        }

        public List<Producer> GetProducers()
        {
            return _producerRepository.Get();
        }
    }
}