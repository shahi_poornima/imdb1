﻿using System;
using System.Collections.Generic;
using System.Text;
using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    public class ActorService
    {
        private readonly ActorRepository _actorRepository;

        public ActorService()
        {
            _actorRepository = new ActorRepository();
        }

        public void AddActor(string name, DateTime Dobid)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            var actor = new Actor()
            {
                Name = name,
                Dob =Dobid
            };

            _actorRepository.Add(actor);
        }

        public List<Actor> GetActors()
        {
            return _actorRepository.Get();
        }
    }
}
