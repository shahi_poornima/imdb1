﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    class Program
    {
        static void Main(string[] args)
        {


            bool Flag = true;
            
            List<string> movieactors = new List<string>();
            //string producers;
            
            var actorObject = new ActorService();
            var movieObject = new MovieService();
            var producerObject = new ProducerService();
            
            movieObject.AddMovie("kgf", 1998, "sample plot", new List<string>(){
                "dfs",
                "rdfs"
            },"Raju");
            actorObject.AddActor("dfs", new DateTime(1998, 05, 20));
            actorObject.AddActor("rbfs", new DateTime(1988, 04, 30));
            producerObject.AddProducer("Raju", new DateTime(1988, 04, 30));

            Console.WriteLine(@"1. List Movies\n2.Add Movie\n3.Add Actor\n4.Add Producer\n5.Delete Movie\n6.Exit");
            while (Flag == true)
            {
                Console.WriteLine("What do you want to do?");
                var selectedOption = Convert.ToInt32(Console.ReadLine());
                switch (selectedOption)
                {
                    case 1:
                        foreach ( var movie in movieObject.GetMovies())
                        {
                            Console.WriteLine("Movie:"+" "+movie.Name);
                            Console.WriteLine("Year:"+" "+movie.Year);
                            Console.WriteLine("Plot"+" "+movie.Plot);
                            Console.Write("Actors:  ");
                            foreach (var actor in movie.Actors)
                            {
                                Console.Write(actor + ',');
                            }
                            Console.WriteLine();
                            Console.Write("Producer: "+" "+movie.Producer);
                            Console.WriteLine();
                        }
                        break;
                    case 2:
                        Console.Write("Name:");
                        var movieName = Console.ReadLine();
                        Console.WriteLine("Year:");
                        var movieYear = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Plot:");
                        var moviePlot = Console.ReadLine();
                        Console.WriteLine("Choose Actors");
                        int actorindex = 1;
                        foreach (var actor in actorObject.GetActors())
                        {
                            Console.WriteLine(actorindex+"."+ actor.Name);
                            actorindex++;

                        }
                        var givenActors = Console.ReadLine().Split(' ');
                        foreach (var actorIndex in givenActors)
                        {
                            movieactors.Add(actorObject.GetActors()[Convert.ToInt32(actorIndex)-1].Name);
                        }
                        
                        Console.WriteLine("Choose Producer:");
                        int producerindex = 1;
                        foreach (var producer in producerObject.GetProducers())
                        {
                            Console.WriteLine(producerindex + "." + producer.Name);
                            producerindex++;
                        }
                        var givenProducers = Console.ReadLine();
                                             
                        movieObject.AddMovie(movieName, movieYear, moviePlot, movieactors,producerObject.GetProducers()
                            [Convert.ToInt32(givenProducers)-1].Name);
                        Console.WriteLine("Movie Added succesfully");

                        break;
                    case 3:
                        Console.WriteLine("Name:");
                        var acName = Console.ReadLine();
                        Console.WriteLine("DOB:");
                        var acDob = Convert.ToDateTime(Console.ReadLine());
                        actorObject.AddActor(acName, acDob);
                        Console.WriteLine("Actor added succesfully");
                        
                        break;
                    case 4:
                        Console.Write("Name:");
                        var producerName = Console.ReadLine();
                        Console.Write("DOB:");
                        var producerDob = Convert.ToDateTime(Console.ReadLine());
                        producerObject.AddProducer(producerName, producerDob);
                        Console.WriteLine("Producer Added Successfully");
                        break;
                    case 5:
                        var movieNameToDelete = Console.ReadLine();
                        movieObject.DeleteMovie(movieNameToDelete);
                        break;
                    case 6:
                        Flag = false;
                        break;
                    default:
                        Console.WriteLine("Enter correct option");
                        break;
                }
            }
        }
    }
}
