﻿using System;
using System.Collections.Generic;

using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    public class MovieService
    {
        private readonly MovieRepository _movieRepository;

        public MovieService()
        {
            _movieRepository = new MovieRepository();
        }

        public void AddMovie(string name, int year, string plot,List<string>actors,string producers)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            Movie movie = new Movie()
            {
                Name = name,
                Year = year,
                Plot = plot,
                Actors=actors,
                Producer=producers
            };

            _movieRepository.Add(movie);
        }

        

        public void DeleteMovie(string name)
        {
            var list = _movieRepository.Get();
            foreach (var movie in list)
            {
                //Console.WriteLine(movie.Name);
                if (name.Equals(movie.Name))
                {
                    _movieRepository.Remove(movie);
                }
            }

        }
        public List<Movie> GetMovies()
        {
            return _movieRepository.Get();
        }
    }
}
