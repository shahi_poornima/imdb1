﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class MovieRepository
    {
        private readonly List<Movie> _movie;
        public MovieRepository()
        {
            _movie = new List<Movie>();
        }

        public void Add(Movie movie)
        {
            _movie.Add(movie);
        }
        public void Remove(Movie movie)
        {
            _movie.Remove(movie);
        }

        public List<Movie> Get()
        {
            return _movie.ToList();
        }
    }
}
