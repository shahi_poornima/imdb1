﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ActorRepository
    {
        private readonly List<Actor> _actors;
        public ActorRepository()
        {
            _actors = new List<Actor>();
        }

        public void Add(Actor actors)
        {
            _actors.Add(actors);
        }

        public List<Actor> Get()
        {
            return _actors.ToList();
        }

    }
}
